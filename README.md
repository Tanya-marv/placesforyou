# PlacesForYou🍕

An App to hepl you to find a place that you like.

## Requirements

Android 6.0+

## Built with

* [Foursquare Api](https://developer.foursquare.com/) - API provides location based experiences with diverse information about venues, users, photos, and check-ins.
* [Jetpack Compose](https://developer.android.com/jetpack/compose) - toolkit for building native UI.
* [Retrofit 2](https://square.github.io/retrofit/) - Library for elegant HTTP Requests.
* [Koin](https://insert-koin.io) - Simple library for dependency injection in Kotlin.
* [Kotlin Coroutines](https://github.com/Kotlin/kotlinx.coroutines) - The new way of doing concurrent tasks in a much simpler manner.
* [Architecture Components](https://developer.android.com/topic/libraries/architecture/) - A useful set of goods for building apps faster.

## TODO

* Add venues menu
* Add tests
* Publish on the Play Store
