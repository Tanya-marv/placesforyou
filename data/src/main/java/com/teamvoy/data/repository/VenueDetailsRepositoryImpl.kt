package com.teamvoy.data.repository

import com.teamvoy.data.network.FoursquareDataSource
import com.teamvoy.domain.model.VenueDetails
import com.teamvoy.domain.repository.VenueDetailsRepository
import javax.inject.Inject

class VenueDetailsRepositoryImpl @Inject constructor(
    private val dataSource: FoursquareDataSource
) : VenueDetailsRepository {

    override suspend fun retrieveVenueDetails(id: String): VenueDetails {
        return dataSource.fetchVenueDetails(id)
    }
}
