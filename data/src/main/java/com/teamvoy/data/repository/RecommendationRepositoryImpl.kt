package com.teamvoy.data.repository

import com.teamvoy.data.network.FoursquareDataSource
import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.domain.repository.RecommendationRepository
import javax.inject.Inject

class RecommendationRepositoryImpl @Inject constructor(
    private val dataSource: FoursquareDataSource
) : RecommendationRepository {

    override suspend fun retrieveRecommendationVenue(
        location: String,
        limit: Int,
        categoryId: String
    ): List<Recommendation> {
        return dataSource.fetchRecommendationVenues(location, limit, categoryId)
    }

    override suspend fun retrieveFoodCategory(): Category {
        return dataSource.fetchFoodCategory()
    }
}
