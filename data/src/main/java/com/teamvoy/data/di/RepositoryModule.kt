package com.teamvoy.data.di

import com.teamvoy.data.repository.RecommendationRepositoryImpl
import com.teamvoy.data.repository.VenueDetailsRepositoryImpl
import com.teamvoy.domain.repository.RecommendationRepository
import com.teamvoy.domain.repository.VenueDetailsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun bindRecommendationRepository(
        impl: RecommendationRepositoryImpl
    ): RecommendationRepository

    @Binds
    fun bindVenueDetailsRepository(
        impl: VenueDetailsRepositoryImpl
    ): VenueDetailsRepository
}
