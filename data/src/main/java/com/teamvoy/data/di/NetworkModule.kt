package com.teamvoy.data.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.teamvoy.data.network.api.FoursquareApi
import com.teamvoy.data.network.interceptor.QueryParamInterceptor
import com.teamvoy.domain.model.ApiBuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    @Provides
    @Singleton
    fun provideQueryParamInterceptor(
        apiBuildConfig: ApiBuildConfig
    ) = QueryParamInterceptor(apiBuildConfig)

    @Provides
    @Singleton
    fun provideOkHttpClient(
        queryParamInterceptor: QueryParamInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(
            HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
        )
        .addInterceptor(queryParamInterceptor)
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        apiBuildConfig: ApiBuildConfig
    ): FoursquareApi = Retrofit.Builder()
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .baseUrl(apiBuildConfig.baseUrl)
        .build()
        .create(FoursquareApi::class.java)
}
