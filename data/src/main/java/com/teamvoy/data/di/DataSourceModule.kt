package com.teamvoy.data.di

import com.teamvoy.data.network.FoursquareDataSource
import com.teamvoy.data.network.impl.FoursquareDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface DataSourceModule {

    @Binds
    fun bindFoursquareDataSource(impl: FoursquareDataSourceImpl): FoursquareDataSource
}
