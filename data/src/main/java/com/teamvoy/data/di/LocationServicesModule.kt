package com.teamvoy.data.di

import com.teamvoy.data.service.LocationServiceImpl
import com.teamvoy.domain.service.LocationService
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface LocationServicesModule {

    @Binds
    fun bindLocationServices(impl: LocationServiceImpl): LocationService
}
