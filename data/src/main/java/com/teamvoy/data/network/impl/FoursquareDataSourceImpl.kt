package com.teamvoy.data.network.impl

import com.teamvoy.data.network.FoursquareDataSource
import com.teamvoy.data.network.api.FoursquareApi
import com.teamvoy.data.network.mapper.toCategory
import com.teamvoy.data.network.mapper.toRecommendationModels
import com.teamvoy.data.network.mapper.toVenueDetails
import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.domain.model.VenueDetails
import javax.inject.Inject

class FoursquareDataSourceImpl @Inject constructor(
    private val api: FoursquareApi
) : FoursquareDataSource {

    override suspend fun fetchRecommendationVenues(
        location: String,
        limit: Int,
        categoryId: String
    ): List<Recommendation> {
        return api.fetchRecommendations(location, limit, categoryId).toRecommendationModels()
    }

    override suspend fun fetchVenueDetails(id: String): VenueDetails {
        return api.fetchVenueDetails(id).toVenueDetails()
    }

    override suspend fun fetchFoodCategory(): Category {
        return api.fetchFoodCategories().response.categories
            .find { it.name == "Food" }!!
            .toCategory()
    }
}
