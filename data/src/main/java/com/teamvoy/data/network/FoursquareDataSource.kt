package com.teamvoy.data.network

import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.domain.model.VenueDetails

interface FoursquareDataSource {

    suspend fun fetchRecommendationVenues(
        location: String,
        limit: Int,
        categoryId: String
    ): List<Recommendation>

    suspend fun fetchVenueDetails(
        id: String
    ): VenueDetails

    suspend fun fetchFoodCategory(): Category
}
