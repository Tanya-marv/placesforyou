package com.teamvoy.data.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FoodResponse(
    @Json(name = "response") val response: FoodCategoriesResponse
) {
    @JsonClass(generateAdapter = true)
    data class FoodCategoriesResponse(
        @Json(name = "categories") val categories: List<CategoryResponse>
    )

    @JsonClass(generateAdapter = true)
    data class CategoryResponse(
        @Json(name = "id") val id: String,
        @Json(name = "name") val name: String,
        @Json(name = "categories") val categories: List<CategoryResponse>
    )
}
