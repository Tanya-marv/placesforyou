package com.teamvoy.data.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VenueDetailsResponse(
    @Json(name = "response") val response: Response
) {
    @JsonClass(generateAdapter = true)
    data class Response(
        @Json(name = "venue") val venue: Venue
    )

    @JsonClass(generateAdapter = true)
    data class Venue(
        @Json(name = "name") val name: String,
        @Json(name = "location") val location: Address,
        @Json(name = "categories") val categories: List<Category>,
        @Json(name = "likes") val likes: Count,
        @Json(name = "rating") val rating: Double?,
        @Json(name = "ratingColor") val ratingColor: String?,
        @Json(name = "description") val description: String?,
        @Json(name = "bestPhoto") val image: Image?
    )

    @JsonClass(generateAdapter = true)
    data class Address(
        @Json(name = "address") val address: String?
    )

    @JsonClass(generateAdapter = true)
    data class Category(
        @Json(name = "name") val name: String,
    )

    @JsonClass(generateAdapter = true)
    data class Count(
        @Json(name = "count") val count: Int
    )

    @JsonClass(generateAdapter = true)
    data class Image(
        @Json(name = "prefix") val prefix: String,
        @Json(name = "suffix") val suffix: String
    )
}
