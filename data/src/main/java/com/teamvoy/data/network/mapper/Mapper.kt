package com.teamvoy.data.network.mapper

import android.location.Location
import com.teamvoy.data.network.model.FoodResponse
import com.teamvoy.data.network.model.RecommendationResponse
import com.teamvoy.data.network.model.VenueDetailsResponse
import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.domain.model.VenueDetails

fun RecommendationResponse.toRecommendationModels(): List<Recommendation> {
    val group = response.groups.find { it.name == "recommended" } ?: return emptyList()
    val models = group.items.map { item ->
        Recommendation(
            id = item.venue.id,
            name = item.venue.name,
            location = item.venue.location.address ?: "",
            categories = item.venue.categories.map { it.name }
        )
    }
    return models
}

fun Location.toLocation(): com.teamvoy.domain.model.Location {
    return com.teamvoy.domain.model.Location(
        longitude = longitude,
        latitude = latitude
    )
}

fun VenueDetailsResponse.toVenueDetails(): VenueDetails {
    val venue = response.venue
    val img = response.venue.image

    return VenueDetails(
        name = venue.name,
        address = venue.location.address,
        categoryName = venue.categories.map { it.name },
        likes = venue.likes.count,
        rating = venue.rating ?: 0.0,
        ratingColor = venue.ratingColor?.let { "#$it" },
        description = venue.description,
        image = img?.run { "${prefix}original$suffix" }
    )
}

fun FoodResponse.CategoryResponse.toCategory(): Category {
    return Category(
        id = id,
        name = name,
        categories = categories.map {
            it.toCategory()
        }
    )
}
