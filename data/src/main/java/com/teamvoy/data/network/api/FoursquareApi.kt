package com.teamvoy.data.network.api

import com.teamvoy.data.network.model.FoodResponse
import com.teamvoy.data.network.model.RecommendationResponse
import com.teamvoy.data.network.model.VenueDetailsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FoursquareApi {

    @GET("explore")
    suspend fun fetchRecommendations(
        @Query("ll") location: String,
        @Query("limit") limit: Int,
        @Query("categoryId") categoryId: String
    ): RecommendationResponse

    @GET("{venueId}")
    suspend fun fetchVenueDetails(
        @Path("venueId") venueId: String
    ): VenueDetailsResponse

    @GET("categories")
    suspend fun fetchFoodCategories(): FoodResponse
}
