package com.teamvoy.data.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RecommendationResponse(
    @Json(name = "response") val response: Response,
) {
    @JsonClass(generateAdapter = true)
    data class Response(
        @Json(name = "groups") val groups: List<Group>
    )

    @JsonClass(generateAdapter = true)
    data class Group(
        @Json(name = "name") val name: String,
        @Json(name = "items") val items: List<Item>
    )

    @JsonClass(generateAdapter = true)
    data class Item(
        @Json(name = "venue") val venue: Venue
    )

    @JsonClass(generateAdapter = true)
    data class Venue(
        @Json(name = "id") val id: String,
        @Json(name = "name") val name: String,
        @Json(name = "location") val location: Location,
        @Json(name = "categories") val categories: List<Category>
    )

    @JsonClass(generateAdapter = true)
    data class Location(
        @Json(name = "address") val address: String?
    )

    @JsonClass(generateAdapter = true)
    data class Category(
        @Json(name = "name") val name: String
    )
}
