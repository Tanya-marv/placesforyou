package com.teamvoy.data.network.interceptor

import com.teamvoy.domain.model.ApiBuildConfig
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class QueryParamInterceptor(
    private val apiBuildConfig: ApiBuildConfig
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()
        val originalHttpUrl: HttpUrl = original.url

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter("client_id", apiBuildConfig.clientId)
            .addQueryParameter("client_secret", apiBuildConfig.clientSecret)
            .addQueryParameter("v", API_VERSION)
            .build()

        val request: Request = original.newBuilder().url(url).build()
        return chain.proceed(request)
    }

    companion object {
        private const val API_VERSION = "20211101"
    }
}
