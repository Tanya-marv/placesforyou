package com.teamvoy.data.service

import android.content.Context
import android.util.Log
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.teamvoy.data.network.mapper.toLocation
import com.teamvoy.domain.model.Location
import com.teamvoy.domain.service.LocationService
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import kotlinx.coroutines.tasks.await

class LocationServiceImpl @Inject constructor(
    @ApplicationContext context: Context
) : LocationService {

    private val fusedLocationProviderClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    override suspend fun getLocation(): Location {
        val location = fusedLocationProviderClient.lastLocation.await().toLocation()
        Log.i("Loc", location.toString())
        return location
    }
}
