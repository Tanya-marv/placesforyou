package com.teamvoy.placesforyou.di

import com.teamvoy.domain.model.ApiBuildConfig
import com.teamvoy.placesforyou.BuildConfig
import com.teamvoy.placesforyou.routing.RoutingChannel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideBuildConfig(): ApiBuildConfig = ApiBuildConfig(
        baseUrl = BuildConfig.FOURSQUARE_API_BASE_URL,
        clientId = BuildConfig.FOURSQUARE_API_CLIENT_ID,
        clientSecret = BuildConfig.FOURSQUARE_API_CLIENT_SECRET
    )

    @Provides
    @Singleton
    fun provideRoutingChannel(): RoutingChannel = RoutingChannel()
}
