package com.teamvoy.placesforyou.routing

enum class Destination {
    RECOMMENDATIONS, DETAILS, FILTER
}
