package com.teamvoy.placesforyou.routing

interface Router {

    fun fromRecommendationToDetails(venueId: String)

    fun fromRecommendationToFilter()

    fun back(result: String)
}
