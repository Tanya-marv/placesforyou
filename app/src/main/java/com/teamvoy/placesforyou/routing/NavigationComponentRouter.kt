package com.teamvoy.placesforyou.routing

import androidx.navigation.NavController

class NavigationComponentRouter(
    private val navController: NavController
) : Router {

    override fun fromRecommendationToDetails(venueId: String) {
        navController.navigate("${Destination.DETAILS}/$venueId")
    }

    override fun fromRecommendationToFilter() {
        navController.navigate(Destination.FILTER)
    }

    override fun back(result: String) {
        navController.previousBackStackEntry
            ?.savedStateHandle
            ?.set("result", result)
        navController.navigateUp()
    }

    private fun NavController.navigate(destination: Destination) {
        navigate(destination.name)
    }
}
