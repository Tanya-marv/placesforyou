package com.teamvoy.placesforyou.routing

import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow

class RoutingChannel {

    private val channel: Channel<(Router) -> Unit> = Channel(
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )

    suspend fun navigate(event: Router.() -> Unit) {
        channel.send(event)
    }

    suspend fun collectWithRouter(router: Router) {
        channel.receiveAsFlow().collect { it(router) }
    }
}
