package com.teamvoy.placesforyou

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navArgument
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.teamvoy.placesforyou.routing.Destination
import com.teamvoy.placesforyou.routing.NavigationComponentRouter
import com.teamvoy.placesforyou.routing.Router
import com.teamvoy.placesforyou.routing.RoutingChannel
import com.teamvoy.placesforyou.routing.composable
import com.teamvoy.placesforyou.ui.screen.details.DetailsScreen
import com.teamvoy.placesforyou.ui.screen.details.DetailsViewModel
import com.teamvoy.placesforyou.ui.screen.filter.FilterScreen
import com.teamvoy.placesforyou.ui.screen.recommendation.RecommendationScreen
import com.teamvoy.placesforyou.ui.screen.recommendation.RecommendationViewModel
import com.teamvoy.placesforyou.ui.theme.PlacesForYouTheme
import dagger.hilt.android.AndroidEntryPoint
import getResult
import javax.inject.Inject

@ExperimentalFoundationApi
@ExperimentalCoilApi
@ExperimentalPermissionsApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject lateinit var detailsFactory: DetailsViewModel.DetailsAssistedFactory
    @Inject lateinit var routingChannel: RoutingChannel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PlacesForYouTheme {
                val navController = rememberNavController()
                val router: Router = NavigationComponentRouter(navController)
                LaunchedEffect(RoutingChannel::class.simpleName) {
                    routingChannel.collectWithRouter(router)
                }
                val systemUiController = rememberSystemUiController()
                val useDarkIcons = MaterialTheme.colors.isLight
                val backgroundColor = MaterialTheme.colors.background
                Surface(color = backgroundColor) {
                    SideEffect {
                        systemUiController.apply {
                            setStatusBarColor(
                                color = backgroundColor,
                                darkIcons = useDarkIcons
                            )
                            setNavigationBarColor(
                                color = backgroundColor,
                                darkIcons = useDarkIcons
                            )
                        }
                    }
                    NavHost(
                        navController = navController,
                        startDestination = Destination.RECOMMENDATIONS.name
                    ) {
                        composable(Destination.RECOMMENDATIONS) {
                            val viewModel = hiltViewModel<RecommendationViewModel>()

                            RecommendationScreen(
                                viewModel = viewModel,
                                router = router
                            )

                            navController.getResult(viewModel::onCategoriesPicked)
                        }

                        composable(
                            route = "${Destination.DETAILS}/{venueId}",
                            arguments = listOf(
                                navArgument("venueId") {
                                    type = NavType.StringType
                                }
                            )
                        ) { backStackEntry ->
                            val venueId = requireNotNull(
                                backStackEntry.arguments?.getString("venueId")
                            ) {
                                "venueId must not be null"
                            }
                            val viewModel = viewModel<DetailsViewModel>(
                                factory = DetailsViewModel.provideFactory(detailsFactory, venueId)
                            )
                            DetailsScreen(
                                viewModel = viewModel
                            )
                        }

                        composable(Destination.FILTER) {
                            FilterScreen(
                                viewModel = hiltViewModel()
                            )
                        }
                    }
                }
            }
        }
    }
}
