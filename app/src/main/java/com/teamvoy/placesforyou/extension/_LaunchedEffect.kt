import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavController

@Composable
fun NavController.getResult(ifExists: (String) -> Unit) {
    LaunchedEffect(Unit) {
        val result = currentBackStackEntry
            ?.savedStateHandle
            ?.get<String>("result")
        Log.i("Result", result ?: "")
        result?.also(ifExists)
    }
}
