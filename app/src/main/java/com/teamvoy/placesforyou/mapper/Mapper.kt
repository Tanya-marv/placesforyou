package com.teamvoy.placesforyou.mapper

import com.teamvoy.domain.model.Category
import com.teamvoy.placesforyou.ui.model.CheckedCategory

fun Category.toCheckedCategory(): CheckedCategory {
    return CheckedCategory(
        id = id,
        name = name,
        categories = categories
    )
}
