package com.teamvoy.placesforyou.ui.screen.details

import com.teamvoy.domain.model.VenueDetails

sealed class DetailsScreenState {

    object Loading : DetailsScreenState()

    object Init : DetailsScreenState()

    data class Success(val details: VenueDetails) : DetailsScreenState()
}
