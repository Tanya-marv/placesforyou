package com.teamvoy.placesforyou.ui.screen.recommendation

import com.teamvoy.domain.model.Recommendation

sealed class RecommendationScreenState {

    object Loading : RecommendationScreenState()

    object Init : RecommendationScreenState()

    data class Success(val recommends: List<Recommendation>) : RecommendationScreenState()
}
