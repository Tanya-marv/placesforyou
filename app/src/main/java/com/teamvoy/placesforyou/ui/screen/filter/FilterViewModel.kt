package com.teamvoy.placesforyou.ui.screen.filter

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teamvoy.domain.interactor.VenueIteractor
import com.teamvoy.placesforyou.mapper.toCheckedCategory
import com.teamvoy.placesforyou.routing.RoutingChannel
import com.teamvoy.placesforyou.ui.model.CheckedCategory
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class FilterViewModel @Inject constructor(
    private val categoryInteractor: VenueIteractor,
    private val routerChannel: RoutingChannel
) : ViewModel() {

    private val _categoriesStateFlow = MutableStateFlow<FilterScreenState>(
        FilterScreenState.Init
    )
    val categoriesStateFlow = _categoriesStateFlow.asStateFlow()

    init {
        viewModelScope.launch {
            _categoriesStateFlow.emit(FilterScreenState.Loading)
            val categories = categoryInteractor.getFoodCategory().categories.map {
                it.toCheckedCategory()
            }
            _categoriesStateFlow.emit(FilterScreenState.Success(categories))
        }
    }

    fun onCategoryChecked(category: CheckedCategory) {
        viewModelScope.launch {
            val categoriesState = categoriesStateFlow.value
            if (categoriesState is FilterScreenState.Success) {
                val oldCategories = categoriesState.categories
                _categoriesStateFlow.emit(
                    FilterScreenState.Success(
                        oldCategories.map {
                            if (it.id == category.id) category
                            else it
                        }
                    )
                )
            }
        }
    }

    fun onApplyCategories() {
        val state = categoriesStateFlow.value
        if (state is FilterScreenState.Success) {
            val ids = state.categories.filter {
                it.checked
            }.joinToString(separator = ",") {
                it.id
            }
            Log.i("Cat", ids)
            viewModelScope.launch {
                routerChannel.navigate {
                    back(ids)
                }
            }
        }
    }
}
