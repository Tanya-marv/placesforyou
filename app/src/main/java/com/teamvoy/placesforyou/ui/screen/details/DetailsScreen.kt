package com.teamvoy.placesforyou.ui.screen.details

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Star
import androidx.compose.material.icons.rounded.ThumbUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.size.OriginalSize
import coil.size.Scale
import com.teamvoy.domain.model.VenueDetails
import com.teamvoy.placesforyou.R
import com.teamvoy.placesforyou.extension.color

@ExperimentalCoilApi
@Composable
fun DetailsScreen(
    viewModel: DetailsViewModel
) {
    when (val state = viewModel.detailsStateFlow.collectAsState().value) {
        is DetailsScreenState.Success -> {
            DetailsView(details = state.details)
        }
        is DetailsScreenState.Loading -> {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircularProgressIndicator(
                    color = Color.Black
                )
            }
        }
        is DetailsScreenState.Init -> { }
    }
}

@ExperimentalCoilApi
@Composable
fun DetailsView(details: VenueDetails) {
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .height(300.dp),
            painter = rememberImagePainter(
                data = if (details.image != null) details.image else R.drawable.ic_image_not_found,
                builder = {
                    crossfade(true)
                    scale(Scale.FIT)
                    size(OriginalSize)
                }
            ),
            contentDescription = null
        )

        Column(
            modifier = Modifier.padding(horizontal = 10.dp)
        ) {
            Text(
                modifier = Modifier
                    .padding(vertical = 10.dp),
                text = details.name,
                fontWeight = FontWeight.Bold
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row {
                    Icon(
                        imageVector = Icons.Rounded.Star,
                        contentDescription = stringResource(R.string.details_rating),
                        tint = details.ratingColor?.color ?: Color.Black
                    )

                    Text(
                        text = details.rating.toString()
                    )
                }

                Row(
                    modifier = Modifier.padding(horizontal = 10.dp)
                ) {
                    Icon(
                        imageVector = Icons.Rounded.ThumbUp,
                        contentDescription = stringResource(R.string.details_likes),
                        tint = Color.Yellow
                    )

                    Text(
                        text = details.likes.toString()
                    )
                }
            }

            details.description?.also {
                Text(
                    modifier = Modifier
                        .padding(vertical = 10.dp),
                    text = stringResource(R.string.details_address, it)
                )
            }

            details.description?.also {
                Text(
                    modifier = Modifier
                        .padding(vertical = 10.dp),
                    text = it
                )
            }

            LazyRow(
                modifier = Modifier
                    .padding(top = 5.dp)
            ) {
                items(details.categoryName) { category ->
                    Card(
                        shape = RoundedCornerShape(25.dp),
                        modifier = Modifier
                            .padding(all = 5.dp),
                        backgroundColor = Color.Black
                    ) {
                        Text(
                            text = category,
                            color = Color.White,
                            modifier = Modifier.padding(horizontal = 15.dp, vertical = 5.dp)
                        )
                    }
                }
            }
        }
    }
}
