package com.teamvoy.placesforyou.ui.screen.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.teamvoy.domain.interactor.VenueIteractor
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailsViewModel @AssistedInject constructor(
    private val detailsInteractor: VenueIteractor,
    @Assisted("venueId") private val venueId: String
) : ViewModel() {

    private val _detailsStateFlow = MutableStateFlow<DetailsScreenState>(DetailsScreenState.Init)
    val detailsStateFlow = _detailsStateFlow.asStateFlow()

    init {
        viewModelScope.launch {
            _detailsStateFlow.emit(DetailsScreenState.Loading)
            val details = detailsInteractor.getVenueDetails(venueId)
            _detailsStateFlow.emit(DetailsScreenState.Success(details))
        }
    }

    @AssistedFactory
    interface DetailsAssistedFactory {

        fun create(@Assisted("venueId") venueId: String): DetailsViewModel
    }

    companion object {

        fun provideFactory(
            assistedFactory: DetailsAssistedFactory,
            venueId: String
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return assistedFactory.create(venueId) as T
            }
        }
    }
}
