package com.teamvoy.placesforyou.ui.screen.recommendation

import android.Manifest
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.FabPosition
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.placesforyou.R
import com.teamvoy.placesforyou.routing.Router

@ExperimentalFoundationApi
@ExperimentalPermissionsApi
@Composable
fun RecommendationScreen(
    viewModel: RecommendationViewModel,
    router: Router
) {
    val locationPermissionState = rememberMultiplePermissionsState(
        listOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    )

    when {
        locationPermissionState.allPermissionsGranted -> {
            ContentRecommendationScreen(viewModel = viewModel, router = router)
        }

        locationPermissionState.shouldShowRationale ||
            !locationPermissionState.permissionRequested -> {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Text(
                    modifier = Modifier
                        .padding(
                            vertical = 200.dp,
                            horizontal = 40.dp
                        )
                        .fillMaxWidth(),
                    fontSize = 18.sp,
                    text = stringResource(R.string.permission_description),
                    textAlign = TextAlign.Justify
                )

                Spacer(modifier = Modifier.height(10.dp))

                Button(
                    modifier = Modifier
                        .width(250.dp)
                        .height(50.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color.Black
                    ),
                    onClick = {
                        locationPermissionState.launchMultiplePermissionRequest()
                    }
                ) {
                    Text(
                        text = stringResource(R.string.btn_request_permission),
                        color = Color.White,
                        fontSize = 18.sp
                    )
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun ContentRecommendationScreen(
    viewModel: RecommendationViewModel,
    router: Router
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(stringResource(R.string.recommendation_in_app_name))
                },
                backgroundColor = Color.White
            )
        },
        content = {
            when (val state = viewModel.recommendationsStateFlow.collectAsState().value) {
                is RecommendationScreenState.Success -> {
                    RecommendationListView(recommends = state.recommends, router = router)
                }
                is RecommendationScreenState.Loading -> {
                    Row(
                        modifier = Modifier.fillMaxSize(),
                        horizontalArrangement = Arrangement.Center,
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        CircularProgressIndicator(
                            color = Color.Black
                        )
                    }
                }
                is RecommendationScreenState.Init -> {
                    viewModel.retrieveRecommend()
                }
            }
        },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    router.fromRecommendationToFilter()
                },
                backgroundColor = Color.Black
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_baseline_filter),
                    contentDescription = stringResource(id = R.string.recommendation_filter),
                    colorFilter = ColorFilter.tint(Color.White)
                )
            }
        }
    )
}

@ExperimentalFoundationApi
@Composable
fun RecommendationListView(recommends: List<Recommendation>, router: Router) {
    CompositionLocalProvider(
        LocalOverScrollConfiguration provides null
    ) {
        LazyColumn(
            modifier = Modifier
                .padding(start = 10.dp, top = 5.dp, end = 10.dp)
        ) {
            items(recommends) { recommend ->
                Card(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(all = 5.dp)
                        .clickable {
                            router.fromRecommendationToDetails(recommend.id)
                        },
                    elevation = 5.dp
                ) {
                    Column(
                        modifier = Modifier
                            .padding(10.dp)
                    ) {
                        Text(
                            text = recommend.name,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier.padding(start = 10.dp)

                        )

                        Text(
                            text = recommend.categories.joinToString(", "),
                            modifier = Modifier.padding(start = 10.dp)
                        )

                        Text(
                            text = recommend.location,
                            modifier = Modifier.padding(start = 10.dp)
                        )
                    }
                }
            }
        }
    }
}
