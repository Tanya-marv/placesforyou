package com.teamvoy.placesforyou.ui.model

import com.teamvoy.domain.model.Category

data class CheckedCategory(
    val id: String,
    val name: String,
    val categories: List<Category>,
    val checked: Boolean = false
)
