package com.teamvoy.placesforyou.ui.screen.recommendation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teamvoy.domain.interactor.VenueIteractor
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class RecommendationViewModel @Inject constructor(
    private val recommendationInteractor: VenueIteractor
) : ViewModel() {

    private val _recommendationsStateFlow = MutableStateFlow<RecommendationScreenState>(
        RecommendationScreenState.Init
    )
    val recommendationsStateFlow = _recommendationsStateFlow.asStateFlow()

    fun retrieveRecommend() {
        viewModelScope.launch {
            _recommendationsStateFlow.emit(RecommendationScreenState.Loading)
            val category = recommendationInteractor.getFoodCategory()
            val recommends = recommendationInteractor.getRecommendations(category.id)
            _recommendationsStateFlow.emit(RecommendationScreenState.Success(recommends))
        }
    }

    fun onCategoriesPicked(ids: String) {
        viewModelScope.launch {
            _recommendationsStateFlow.emit(RecommendationScreenState.Loading)
            val recommends = recommendationInteractor.getRecommendations(ids)
            _recommendationsStateFlow.emit(RecommendationScreenState.Success(recommends))
        }
    }
}
