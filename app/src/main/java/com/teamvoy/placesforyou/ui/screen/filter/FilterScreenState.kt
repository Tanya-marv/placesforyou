package com.teamvoy.placesforyou.ui.screen.filter

import com.teamvoy.placesforyou.ui.model.CheckedCategory

sealed class FilterScreenState {

    object Loading : FilterScreenState()

    object Init : FilterScreenState()

    data class Success(val categories: List<CheckedCategory>) : FilterScreenState()
}
