package com.teamvoy.placesforyou.ui.screen.filter

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Checkbox
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.teamvoy.placesforyou.R
import com.teamvoy.placesforyou.ui.model.CheckedCategory

@ExperimentalFoundationApi
@Composable
fun FilterScreen(
    viewModel: FilterViewModel
) {
    when (val state = viewModel.categoriesStateFlow.collectAsState().value) {
        is FilterScreenState.Success -> {
            FilterView(
                categories = state.categories,
                onCategoryChecked = viewModel::onCategoryChecked,
                onApplyCategory = viewModel::onApplyCategories
            )
        }
        is FilterScreenState.Loading -> {
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                CircularProgressIndicator(
                    color = Color.Black
                )
            }
        }
        is FilterScreenState.Init -> {
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun FilterView(
    categories: List<CheckedCategory>,
    onCategoryChecked: (CheckedCategory) -> Unit,
    onApplyCategory: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        Button(
            modifier = Modifier
                .padding(all = 10.dp)
                .width(250.dp)
                .height(50.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color.Black
            ),
            onClick = onApplyCategory
        ) {
            Text(
                text = stringResource(R.string.filter_apply),
                color = Color.White,
                fontSize = 18.sp
            )
        }

        CompositionLocalProvider(
            LocalOverScrollConfiguration provides null
        ) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(start = 10.dp, end = 10.dp)
            ) {
                items(categories) { category ->

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                horizontal = 10.dp,
                                vertical = 10.dp
                            )
                    ) {
                        Checkbox(
                            checked = category.checked,
                            onCheckedChange = { isChecked ->
                                onCategoryChecked(
                                    category.copy(
                                        checked = isChecked
                                    )
                                )
                            }
                        )

                        Spacer(
                            modifier = Modifier
                                .width(15.dp)
                        )

                        Text(
                            text = category.name,
                            fontSize = 18.sp
                        )
                    }
                }
            }
        }
    }
}
