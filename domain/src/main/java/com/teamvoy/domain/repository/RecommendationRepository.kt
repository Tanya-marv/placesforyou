package com.teamvoy.domain.repository

import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation

interface RecommendationRepository {

    suspend fun retrieveRecommendationVenue(
        location: String,
        limit: Int,
        categoryId: String
    ): List<Recommendation>

    suspend fun retrieveFoodCategory(): Category
}
