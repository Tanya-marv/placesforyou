package com.teamvoy.domain.repository

import com.teamvoy.domain.model.VenueDetails

interface VenueDetailsRepository {

    suspend fun retrieveVenueDetails(id: String): VenueDetails
}
