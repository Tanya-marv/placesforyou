package com.teamvoy.domain.di

import com.teamvoy.domain.interactor.VenueIteractor
import com.teamvoy.domain.interactor.impl.VenueInteractorImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface InteractorModule {

    @Binds
    fun bindRecommendationInteractor(impl: VenueInteractorImpl): VenueIteractor
}
