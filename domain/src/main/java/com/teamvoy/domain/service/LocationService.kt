package com.teamvoy.domain.service

import com.teamvoy.domain.model.Location

interface LocationService {

    suspend fun getLocation(): Location
}
