package com.teamvoy.domain.interactor

import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.domain.model.VenueDetails

interface VenueIteractor {

    suspend fun getRecommendations(categoryId: String): List<Recommendation>

    suspend fun getVenueDetails(id: String): VenueDetails

    suspend fun getFoodCategory(): Category
}
