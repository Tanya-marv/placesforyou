package com.teamvoy.domain.interactor.impl

import com.teamvoy.domain.interactor.VenueIteractor
import com.teamvoy.domain.model.Category
import com.teamvoy.domain.model.Recommendation
import com.teamvoy.domain.model.VenueDetails
import com.teamvoy.domain.repository.RecommendationRepository
import com.teamvoy.domain.repository.VenueDetailsRepository
import com.teamvoy.domain.service.LocationService
import javax.inject.Inject

class VenueInteractorImpl @Inject constructor() : VenueIteractor {

    @Inject lateinit var recommendationRepository: RecommendationRepository
    @Inject lateinit var venueDetailsRepository: VenueDetailsRepository
    @Inject lateinit var locationServices: LocationService

    override suspend fun getRecommendations(categoryId: String): List<Recommendation> {
        return recommendationRepository.retrieveRecommendationVenue(
            location = "${locationServices.getLocation().latitude}," +
                "${locationServices.getLocation().longitude}",
            limit = LIMIT_RECOMMENDATION,
            categoryId = categoryId
        )
    }

    override suspend fun getVenueDetails(id: String): VenueDetails {
        return venueDetailsRepository.retrieveVenueDetails(id)
    }

    override suspend fun getFoodCategory(): Category {
        return recommendationRepository.retrieveFoodCategory()
    }

    companion object {
        private const val LIMIT_RECOMMENDATION: Int = 10
    }
}
