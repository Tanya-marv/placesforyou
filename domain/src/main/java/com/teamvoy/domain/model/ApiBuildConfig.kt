package com.teamvoy.domain.model

data class ApiBuildConfig(
    val baseUrl: String,
    val clientId: String,
    val clientSecret: String
)
