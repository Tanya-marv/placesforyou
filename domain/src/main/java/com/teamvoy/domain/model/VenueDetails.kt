package com.teamvoy.domain.model

data class VenueDetails(
    val name: String,
    val categoryName: List<String>,
    val address: String?,
    val rating: Double,
    val ratingColor: String?,
    val likes: Int,
    val description: String?,
    val image: String?
)
