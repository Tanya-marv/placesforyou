package com.teamvoy.domain.model

data class Location(
    val longitude: Double,
    val latitude: Double
)
