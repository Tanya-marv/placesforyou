package com.teamvoy.domain.model

data class Category(
    val id: String,
    val name: String,
    val categories: List<Category>
)
