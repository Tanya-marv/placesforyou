package com.teamvoy.domain.model

data class Recommendation(
    val id: String,
    val name: String,
    val location: String,
    val categories: List<String>
)
